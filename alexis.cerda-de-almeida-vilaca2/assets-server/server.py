## CE SCRIPT PERMET DE STOCKER LES ASSETS TROP LOURD SUR UN SERVEUR TIER ET LES RECUPERER LORS DU CHARGEMENT DE LA PAGE AFIN D'OPTIMISER L'ESPACE LOCALEMENT. IL EST EXECUTER SUR UN SERVEUR TIERS AUTO HEBERGE ##

from flask import Flask, send_from_directory, abort

app = Flask(__name__)


IMAGE_DIRECTORY = "assets"

@app.route('/images/<filename>', methods=['GET'])
def serve_image(filename):
    try:
        return send_from_directory(IMAGE_DIRECTORY, filename)
    except FileNotFoundError:
        abort(404)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7079)
